# Demo of the application

![Game starts](./images/game_start.png "Start game")
![First try searching](./images/searching.png "Searching process")
![Search success](./images/search_success.png "Successful searching")  
![Search failure](./images/search_failure.png "Unsuccessful searching")
![Hmmble won](./images/hmmble_won.png "The app guessed a song and won")
![User won](./images/user_won.png "The didn't guess the song. You won")
![Score](./images/score.png "Score")
![Songs history list](./images/history_list.png "Songs history. List view")
![Songs history grid](./images/history_grid.png "Songs history. Grid view")
![Open in menu](./images/open_in.png "Song's 'open in' bottom sheet menu")
