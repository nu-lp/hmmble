# Hmmble

An application guesses songs. The app has 5 attempts to guess a song: if Hmmble guesses - its won,
you otherwise. Songs history is saved and later you can open a song in Apple Music, Deezer or on
https://lis.tn

**See [Demo](DEMO.md)**

Flow:
* Hmmble records song then searches it
* Shows you result where you can listen to song's preview and decide
  whether Hmmble's variant is right
* In case of Hmmble guesses one point goes to it. If not - Hmmble does
  the next attemt (max 5).

## Getting Started

### Prerequisites

* Download and install
  [Android Studio](https://developer.android.com/studio)
* The application uses [Audd API](https://docs.audd.io/), that's why you
  need an `api_token`. Get one from their
  [Telegram Bot](https://t.me/auddbot?start=api)

### Installing

* Clone the repository `git clone: https://gitlab.com/nu-lp/hmmble.git`
* Create a Firebase project at
  [Firebase Console](https://console.firebase.google.com). *You could
  skip it if you don't want to receive crash reports.*
  * Create Android application with `ua.lpnu.hmmble` as package name
  * Download `google-services.json` and put it to
    `folder_you_clone_project/app/`
* Inside folder you have cloned the repository create `keys.properties`
  and write to it your Audd `api_token` like `AUDD_API_TOKEN=your_token`
* Start Android Studio, open your source code folder and check if the
  Gradle build will be successful
* Connect your phone to the computer. **Notice that application works
  only with a real device due to `Media recorder` limitation. See
  [MediaRecorder docs](https://developer.android.com/guide/topics/media/mediarecorder)**
* If the build is successful, you can run the app by doing the
  following: click **Run** -> **Run 'app'**

## Built With

* [Firebase Crashlytics](https://firebase.google.com/docs/crashlytics) - Crash reporter
* [Moshi](https://github.com/square/moshi) - JSON library
* [Retrofit](https://square.github.io/retrofit/) - A type-safe HTTP client

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details