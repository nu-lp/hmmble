package ua.lpnu.hmmble

import android.app.Application
import timber.log.Timber

class HmmbleApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
    }
}