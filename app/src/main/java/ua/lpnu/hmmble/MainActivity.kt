package ua.lpnu.hmmble

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import ua.lpnu.hmmble.databinding.ActivityMainBinding
import ua.lpnu.hmmble.utils.GameReminderUtil

class MainActivity : AppCompatActivity() {
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var appBarConfig: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
        val navFragment = supportFragmentManager.findFragmentById(R.id.navHostFragment) as NavHostFragment
        val navController = navFragment.navController

        setSupportActionBar(binding.toolbar)

        val topLevelDestinations = setOf(
            R.id.scoreFragment,
            R.id.songsHistoryFragment,
            R.id.startGameFragment,
            R.id.searchFragment,
            R.id.searchingFailureFragment,
            R.id.searchingSuccessFragment,
            R.id.hmmbleWonFragment,
            R.id.userWonFragment
        )
        drawerLayout = binding.drawerLayout
        appBarConfig = AppBarConfiguration(topLevelDestinations, drawerLayout)

        binding.toolbar.setupWithNavController(navController, appBarConfig)
        NavigationUI.setupWithNavController(binding.navView, navController)
    }

    override fun onStart() {
        super.onStart()
        GameReminderUtil.cancelReminder(applicationContext!!)
    }

    override fun onStop() {
        super.onStop()
        GameReminderUtil.setupReminder(applicationContext!!)
    }
}
