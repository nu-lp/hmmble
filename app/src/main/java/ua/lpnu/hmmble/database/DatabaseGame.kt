package ua.lpnu.hmmble.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Games")
data class DatabaseGame(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0L,
    var isUserWon: Boolean = false
)