package ua.lpnu.hmmble.database

import androidx.room.DatabaseView

@DatabaseView(
    value = "SELECT COUNT(id) AS totalGames, COUNT(NULLIF(0, isUserWon)) AS userScore, COUNT(NULLIF(1, isUserWon)) AS hmmbleScore FROM Games",
    viewName = "Score"
)
data class DatabaseScore(
    val totalGames: Long,
    val userScore: Long,
    val hmmbleScore: Long
)