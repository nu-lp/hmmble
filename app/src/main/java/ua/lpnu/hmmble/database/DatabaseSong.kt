package ua.lpnu.hmmble.database

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import ua.lpnu.hmmble.domain.Song

@Entity(tableName = "Songs", indices = [Index(value = ["title", "artist"], unique = true)])
data class DatabaseSong(
    val title: String,
    val artist: String,
    val listenLinkUrl: String,
    val appleMusicUrl: String?,
    val deezerUrl: String?,
    val previewUrl: String?,
    val albumImgUrl: String?,
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0L
)

fun DatabaseSong.asDomainModel(): Song {
    return Song(
        title,
        artist,
        listenLinkUrl,
        null,
        appleMusicUrl,
        deezerUrl,
        previewUrl,
        albumImgUrl,
        id
    )
}