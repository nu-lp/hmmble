package ua.lpnu.hmmble.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface GameDao {
    @Insert
    fun insert(game: DatabaseGame)

    @Update
    fun update(game: DatabaseGame)

    @Query("SELECT * FROM Games ORDER BY id DESC LIMIT 1")
    fun getLastGame(): DatabaseGame

    @Query("DELETE FROM Games")
    fun resetScore()

    @Query("SELECT * FROM Score")
    fun getScore(): LiveData<DatabaseScore>
}