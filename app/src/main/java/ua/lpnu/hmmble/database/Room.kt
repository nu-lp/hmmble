package ua.lpnu.hmmble.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(
    entities = [DatabaseSong::class, DatabaseGame::class],
    exportSchema = false,
    version = 2,
    views = [DatabaseScore::class]
)
abstract class GameDatabase : RoomDatabase() {
    abstract val songDao: SongDao
    abstract val gameDao: GameDao

    companion object {
        private lateinit var INSTANCE: GameDatabase

        fun getDatabase(context: Context): GameDatabase {

            synchronized(GameDatabase::class.java) {
                if (!::INSTANCE.isInitialized) {
                    INSTANCE = Room.databaseBuilder(
                        context,
                        GameDatabase::class.java,
                        "game_database"
                    ).build()
                }
            }
            return INSTANCE
        }
    }
}