package ua.lpnu.hmmble.database

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface SongDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(song: DatabaseSong): Long

    @Query("SELECT * FROM Songs ORDER BY id DESC")
    fun getAllSongs(): LiveData<List<DatabaseSong>>

    @Query("SELECT * FROM Songs WHERE id = :id")
    fun getSong(id: Long): DatabaseSong

    @Delete
    fun deleteSong(song: DatabaseSong)

    @Query("DELETE FROM Songs")
    fun clearSongsHistory()
}