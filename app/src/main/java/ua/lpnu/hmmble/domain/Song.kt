package ua.lpnu.hmmble.domain

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import ua.lpnu.hmmble.database.DatabaseSong

@Parcelize
data class Song(
    val title: String,
    val artist: String,
    val listenLinkUrl: String,
    val lyrics: String?,
    val appleMusicUrl: String?,
    val deezerUrl: String?,
    val previewUrl: String?,
    val albumImgUrl: String?,
    val id: Long = -1L
) : Parcelable

fun Song.asDatabaseModel(): DatabaseSong {
    return DatabaseSong(
        title,
        artist,
        listenLinkUrl,
        appleMusicUrl,
        deezerUrl,
        previewUrl,
        albumImgUrl
    )
}