package ua.lpnu.hmmble.elements.songopenin

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import timber.log.Timber
import ua.lpnu.hmmble.R
import ua.lpnu.hmmble.database.GameDatabase
import ua.lpnu.hmmble.databinding.FragmentOpenInBottomSheetBinding

/**
 * A simple [Fragment] subclass.
 */
class OpenInBottomSheetFragment : BottomSheetDialogFragment() {
    companion object {
        const val TAG = "OpenInBottomSheet"
        const val SONG_ID_ARG = "songIdArg"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val songId = requireArguments().getLong(SONG_ID_ARG)
        Timber.d("Accepted song id: %d", songId)

        val dataSource = GameDatabase
            .getDatabase(requireActivity().application)
            .songDao
        val factory = OpenInBottomSheetViewModelFactory(dataSource)
        val viewModel = ViewModelProvider(this, factory).get(OpenInBottomSheetViewModel::class.java)
        val binding = FragmentOpenInBottomSheetBinding.inflate(inflater, container, false)

        viewModel.fetchSong(songId)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel

//        Click listeners
        viewModel.openInIntent.observe(this, Observer {
            it?.let {
                if (null != it.resolveActivity(requireActivity().packageManager)) {
                    startActivity(it)
                } else {
                    Timber.i("No application to do [%s] with data [%s]", it.action, it.data.toString())
                    Toast.makeText(context, R.string.no_app_to_do_action, Toast.LENGTH_LONG).show()
                }
            }
            dismiss()
        })

        return binding.root
    }

}
