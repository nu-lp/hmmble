package ua.lpnu.hmmble.elements.songopenin

import android.content.Intent
import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import ua.lpnu.hmmble.database.DatabaseSong
import ua.lpnu.hmmble.database.SongDao
import ua.lpnu.hmmble.database.asDomainModel
import ua.lpnu.hmmble.domain.Song

class OpenInBottomSheetViewModel(val songDao: SongDao) : ViewModel() {
    private val job = Job()
    private val scope = CoroutineScope(Dispatchers.Main + job)

    private val _song = MutableLiveData<DatabaseSong>()
    val song: LiveData<Song>
        get() = Transformations.map(_song) {
            it?.asDomainModel()
        }

    private val _openInIntent = MutableLiveData<Intent>()
    val openInIntent: LiveData<Intent>
        get() = _openInIntent

    private suspend fun fetchSongDatabase(id: Long): DatabaseSong {
        return withContext(Dispatchers.IO) {
            songDao.getSong(id)
        }
    }

    private suspend fun deleteSongDatabase() {
        withContext(Dispatchers.IO) {
            _song.value?.let {
                songDao.deleteSong(it)
            }
        }
    }

    fun fetchSong(id: Long) {
        scope.launch {
            _song.value = fetchSongDatabase(id)
        }
    }

    fun deleteSong() {
        scope.launch {
            deleteSongDatabase()
            _openInIntent.value = null
        }
    }

    fun openInBrowser() {
        _song.value?.listenLinkUrl?.let {
            _openInIntent.value = Intent(Intent.ACTION_VIEW, Uri.parse(it))
        }
    }

    fun openInAppleMusic() {
        _song.value?.appleMusicUrl?.let {
            _openInIntent.value = Intent(Intent.ACTION_VIEW, Uri.parse(it))
        }
    }

    fun openInDeezer() {
        _song.value?.deezerUrl?.let {
            _openInIntent.value = Intent(Intent.ACTION_VIEW, Uri.parse(it))
        }
    }

    fun shareSong() {
        _song.value?.let {
            val content = "${it.title} - ${it.artist}\n${it.listenLinkUrl}"
            val sentIntent = Intent(Intent.ACTION_SEND).apply {
                putExtra(Intent.EXTRA_TEXT, content)
                type = "text/plain"
            }
            _openInIntent.value = Intent.createChooser(sentIntent, null)
        }
    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
}