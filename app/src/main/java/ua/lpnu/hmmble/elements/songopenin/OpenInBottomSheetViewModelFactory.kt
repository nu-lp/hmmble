package ua.lpnu.hmmble.elements.songopenin

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ua.lpnu.hmmble.database.SongDao

class OpenInBottomSheetViewModelFactory(private val songDao: SongDao) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(OpenInBottomSheetViewModel::class.java)) {
            return OpenInBottomSheetViewModel(songDao) as T
        }
        throw IllegalArgumentException("Unknown view model")
    }
}