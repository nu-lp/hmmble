package ua.lpnu.hmmble.network

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import ua.lpnu.hmmble.BuildConfig

private const val BASE_URL = "https://api.audd.io/"

interface AuddApiService {
    @Multipart
    @POST("?api_token=${BuildConfig.AUDD_API_KEY}&return=apple_music,deezer,lyrics")
    fun audioRecognition(@Part file: MultipartBody.Part): Call<AuddResponseDto>
}

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

object Network {
    private val retrofit = Retrofit.Builder()
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .baseUrl(BASE_URL)
        .build()

    val auddService: AuddApiService by lazy {
        retrofit.create(AuddApiService::class.java)
    }
}
