package ua.lpnu.hmmble.network

import com.squareup.moshi.Json

enum class Status {
    @Json(name = "success")
    SUCCESS,

    @Json(name = "error")
    ERROR
}

data class AuddResponseDto(
    val status: Status,
    val result: SongDto?,
    val error: ErrorDto?
)