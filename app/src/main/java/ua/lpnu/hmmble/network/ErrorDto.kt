package ua.lpnu.hmmble.network

import com.squareup.moshi.Json

data class ErrorDto(
    @Json(name = "error_code")
    val errorCode: Int,
    @Json(name = "error_message")
    val errorMessage: String
)