package ua.lpnu.hmmble.network

import com.squareup.moshi.Json
import ua.lpnu.hmmble.domain.Song

data class SongDto(
    val title: String,
    val artist: String,
    @Json(name = "song_link")
    val listenLinkUrl: String,
    val lyrics: LyricsDto?,
    @Json(name = "apple_music")
    val appleMusic: AppleMusicDto?,
    val deezer: DeezerDto?
)

data class LyricsDto(val lyrics: String)

data class AppleMusicDto(val url: String)

data class DeezerDto(
    @Json(name = "link")
    val url: String,
    @Json(name = "preview")
    val previewUrl: String,
    val album: AlbumDto
)

data class AlbumDto(
    @Json(name = "cover_medium")
    val imgUrl: String
)

fun SongDto.asDomainModel(): Song {
    var deezerUrl: String? = null
    var previewUrl: String? = null
    var albumImgUrl: String? = null

    deezer?.let {
        deezerUrl = it.url
        previewUrl = it.previewUrl
        albumImgUrl = it.album.imgUrl
    }

    return Song(
        title,
        artist,
        listenLinkUrl,
        lyrics?.lyrics,
        appleMusic?.url,
        deezerUrl,
        previewUrl,
        albumImgUrl
    )
}