package ua.lpnu.hmmble.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import ua.lpnu.hmmble.R
import ua.lpnu.hmmble.utils.GameReminderUtil
import ua.lpnu.hmmble.utils.NotificationUtil

class GameReminderReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        // Setup reminder again
        GameReminderUtil.setupReminder(context)

        val channelId = NotificationUtil.createNotificationChannel(
            context,
            NotificationUtil.NotificationData.createGameReminder(context)
        )
        val notification = NotificationCompat.Builder(context, channelId)
            .setSmallIcon(R.drawable.ic_headset_black_24dp)
            .setContentTitle(context.getString(R.string.lets_play_again))
            .setContentText(context.getString(R.string.you_have_not_played_for_n_days))
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setDefaults(NotificationCompat.DEFAULT_ALL)
            .build()

        with(NotificationManagerCompat.from(context)) {
            notify(NotificationUtil.GAME_REMINDER_ID, notification)
        }
    }
}
