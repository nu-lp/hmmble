package ua.lpnu.hmmble.screens.game.hmmblewon

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import ua.lpnu.hmmble.databinding.HmmbleWonFragmentBinding

class HmmbleWonFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = HmmbleWonFragmentBinding.inflate(inflater, container, false)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.newGameBtn.setOnClickListener {
            findNavController().navigate(HmmbleWonFragmentDirections.actionHmmbleWonFragmentToStartGameFragment())
        }

        return binding.root
    }
}
