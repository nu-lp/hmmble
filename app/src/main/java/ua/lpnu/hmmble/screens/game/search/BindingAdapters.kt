package ua.lpnu.hmmble.screens.game.search

import android.view.View
import androidx.databinding.BindingAdapter

@BindingAdapter("isVisible")
fun changeVisibility(view: View, isVisible: Boolean) {
    view.visibility = when (isVisible) {
        true -> View.VISIBLE
        false -> View.INVISIBLE
    }
}

@BindingAdapter("isPresent")
fun changePresence(view: View, isPresent: Boolean) {
    view.visibility = when (isPresent) {
        true -> View.VISIBLE
        false -> View.GONE
    }
}