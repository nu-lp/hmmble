package ua.lpnu.hmmble.screens.game.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import timber.log.Timber
import ua.lpnu.hmmble.R
import ua.lpnu.hmmble.databinding.SearchFragmentBinding

class SearchFragment : Fragment() {
    companion object {
        /**
         * Specifies max amount of tries for guessing song
         */
        const val MAX_TRY = 5
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val args = SearchFragmentArgs.fromBundle(requireArguments())
        if (args.numberTry > MAX_TRY) {
            findNavController().navigate(SearchFragmentDirections.actionSearchFragmentToUserWonFragment())
            Timber.d("User won")
        }

        val binding = SearchFragmentBinding.inflate(inflater, container, false)
        val viewModel = ViewModelProvider(this).get(SearchViewModel::class.java)

        binding.viewModel = viewModel
        binding.numberTryText.text = getNumberTryText(args.numberTry)
        binding.lifecycleOwner = viewLifecycleOwner

        viewModel.navigateFailureSearch.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().navigate(
                    SearchFragmentDirections.actionAudioModeFragmentToSearchingFailureFragment(
                        args.numberTry
                    )
                )
                viewModel.doneNavigateFailureSearch()
                Timber.d("Song has not been found")
            }
        })
        viewModel.navigateSuccessfulSearch.observe(viewLifecycleOwner, Observer {
            if (null != it) {
                findNavController().navigate(
                    SearchFragmentDirections.actionAudioModeFragmentToSearchingSuccessFragment(args.numberTry, it)
                )
                viewModel.doneNavigateSuccessfulSearch()
                Timber.d("Song has been found: %s", it.toString())
            }
        })
        viewModel.startRecording()

        return binding.root
    }

    private fun getNumberTryText(number: Int): String {
        return getString(
            when (number) {
                1 -> R.string.search_try_1
                2 -> R.string.search_try_2
                3 -> R.string.search_try_3
                4 -> R.string.search_try_4
                5 -> R.string.search_try_5
                else -> R.string.search_try_1
            }
        )
    }

}
