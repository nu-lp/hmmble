package ua.lpnu.hmmble.screens.game.search

import android.media.MediaRecorder
import android.os.CountDownTimer
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.crashlytics.FirebaseCrashlytics
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import ua.lpnu.hmmble.domain.Song
import ua.lpnu.hmmble.network.AuddResponseDto
import ua.lpnu.hmmble.network.Network
import ua.lpnu.hmmble.network.Status
import ua.lpnu.hmmble.network.asDomainModel
import java.io.File

class SearchViewModel : ViewModel() {
    companion object {
        const val ONE_SECOND = 1_000L
        const val RECORD_DURATION = 6_000L
    }

    private val mediaRecorder = MediaRecorder()
    private val timer: CountDownTimer
    private lateinit var tmpAudioFile: File

    private val _isSongSearching = MutableLiveData<Boolean>()
    val isSongSearching: LiveData<Boolean>
        get() = _isSongSearching

    private val _navigateSuccessfulSearch = MutableLiveData<Song>()
    val navigateSuccessfulSearch: LiveData<Song>
        get() = _navigateSuccessfulSearch

    private val _navigateFailureSearch = MutableLiveData<Boolean>()
    val navigateFailureSearch: LiveData<Boolean>
        get() = _navigateFailureSearch

    init {
        timer = object : CountDownTimer(RECORD_DURATION, ONE_SECOND) {
            override fun onFinish() {
                mediaRecorder.stop()
                mediaRecorder.reset()
                searchAudio()
            }

            override fun onTick(millisUntilFinished: Long) {}
        }
    }

    private fun searchAudio() {
        _isSongSearching.value = true
        val filePart = RequestBody.create(MediaType.parse("audio/mp4"), tmpAudioFile)
        val part = MultipartBody.Part.createFormData("file", tmpAudioFile.name, filePart)

        Network.auddService.audioRecognition(part)
            .enqueue(object : Callback<AuddResponseDto> {
                override fun onFailure(call: Call<AuddResponseDto>, t: Throwable) {
                    _isSongSearching.value = false
                    FirebaseCrashlytics.getInstance().setCustomKey("audd_err", t.message ?: "")
                    FirebaseCrashlytics.getInstance().log("Audd request error")
                    Timber.e("Song searching failed: %s", t.message)
                    _navigateFailureSearch.value = true
                }

                override fun onResponse(
                    call: Call<AuddResponseDto>, response: Response<AuddResponseDto>
                ) {
                    _isSongSearching.value = false
                    /*
                    Even if error, API return object. So check status on error,
                    in case of success check result
                     */
                    val result = response.body()!!.run {
                        Timber.d("Search status %s", status.toString())

                        when (status) {
                            Status.SUCCESS -> result?.asDomainModel()
                            else -> {
                                Timber.e("API error response: %s", error?.toString())
                                FirebaseCrashlytics.getInstance().setCustomKey("audd_err", error?.toString() ?: "")
                                FirebaseCrashlytics.getInstance().log("Audd request error")
                                null
                            }
                        }
                    }
                    Timber.i("Search ended. Song: %s", result?.title)
                    if (null != result) {
                        _navigateSuccessfulSearch.value = result
                    } else {
                        _navigateFailureSearch.value = true
                    }
                }
            })
    }

    fun startRecording() {
        tmpAudioFile = createTempFile()

        mediaRecorder.apply {
            setAudioSource(MediaRecorder.AudioSource.MIC)
            setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
            setOutputFile(tmpAudioFile.outputStream().fd)
            setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB)
            prepare()
            start()
        }

        timer.start()
        Timber.i("Start recording...")
    }

    fun doneNavigateFailureSearch() {
        _navigateFailureSearch.value = false
    }

    fun doneNavigateSuccessfulSearch() {
        _navigateSuccessfulSearch.value = null
    }

    override fun onCleared() {
        super.onCleared()
        timer.cancel()
        mediaRecorder.release()
    }
}
