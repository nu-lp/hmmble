package ua.lpnu.hmmble.screens.game.searchingfailure

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import ua.lpnu.hmmble.databinding.SearchingFailureFragmentBinding

class SearchingFailureFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = SearchingFailureFragmentBinding.inflate(inflater, container, false)
        val args = SearchingFailureFragmentArgs.fromBundle(requireArguments())

        binding.nextTryBtn.setOnClickListener {
            findNavController().navigate(
                SearchingFailureFragmentDirections.actionSearchingFailureFragmentToSearchFragment(
                    args.numberTry + 1
                )
            )
        }

        return binding.root
    }
}
