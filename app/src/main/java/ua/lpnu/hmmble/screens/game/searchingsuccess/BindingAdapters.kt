package ua.lpnu.hmmble.screens.game.searchingsuccess

import android.widget.ImageButton
import android.widget.TextView
import androidx.databinding.BindingAdapter
import ua.lpnu.hmmble.R

@BindingAdapter("isPlaying")
fun ImageButton.playPauseIcon(isPlaying: Boolean?) {
    setImageResource(
        when (isPlaying) {
            true -> R.drawable.ic_pause_black_24dp
            false -> R.drawable.ic_play_arrow_black_24dp
            else -> R.drawable.loading_anim
        }
    )
}

@BindingAdapter("lyrics")
fun TextView.setLyrics(lyrics: String?) {
    if (null != lyrics?.takeIf(String::isNotBlank)) {
        text = lyrics
    } else {
        setText(R.string.no_lyrics_for_song)
    }
}