package ua.lpnu.hmmble.screens.game.searchingsuccess

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import timber.log.Timber
import ua.lpnu.hmmble.database.GameDatabase
import ua.lpnu.hmmble.databinding.SearchingSuccessFragmentBinding

class SearchingSuccessFragment : Fragment() {
    private lateinit var viewModel: SearchingSuccessViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val dataSource = GameDatabase
            .getDatabase(requireActivity().application)
            .songDao
        val factory = SearchingSuccessViewModelFactory(dataSource)
        viewModel = ViewModelProvider(this, factory).get(SearchingSuccessViewModel::class.java)
        val args = SearchingSuccessFragmentArgs.fromBundle(requireArguments())
        val binding = SearchingSuccessFragmentBinding.inflate(inflater, container, false)

        viewModel.saveSong(args.song)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.song = args.song
        binding.viewModel = viewModel
        binding.noBtn.setOnClickListener {
            findNavController().navigate(
                SearchingSuccessFragmentDirections.actionSearchingSuccessFragmentToSearchFragment(
                    args.numberTry + 1
                )
            )
        }
        binding.yesBtn.setOnClickListener {
            findNavController().navigate(SearchingSuccessFragmentDirections.actionSearchingSuccessFragmentToHmmbleWonFragment())
            Timber.d("Hmmble won")
        }

        return binding.root
    }

    override fun onStop() {
        super.onStop()
        viewModel.pauseSong()
    }
}
