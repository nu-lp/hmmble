package ua.lpnu.hmmble.screens.game.searchingsuccess

import android.media.AudioAttributes
import android.media.MediaPlayer
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.crashlytics.FirebaseCrashlytics
import kotlinx.coroutines.*
import timber.log.Timber
import ua.lpnu.hmmble.database.DatabaseSong
import ua.lpnu.hmmble.database.SongDao
import ua.lpnu.hmmble.domain.Song
import ua.lpnu.hmmble.domain.asDatabaseModel

class SearchingSuccessViewModel(val database: SongDao) : ViewModel(),
    MediaPlayer.OnPreparedListener,
    MediaPlayer.OnErrorListener,
    MediaPlayer.OnCompletionListener {

    private val mediaAttributes = AudioAttributes.Builder()
        .setUsage(AudioAttributes.USAGE_MEDIA)
        .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
        .build()
    private val mediaPlayer = MediaPlayer().apply {
        setOnPreparedListener(this@SearchingSuccessViewModel)
        setOnCompletionListener(this@SearchingSuccessViewModel)
        setOnErrorListener(this@SearchingSuccessViewModel)
    }

    private val job = Job()
    private val scope = CoroutineScope(Dispatchers.Main + job)

    private val _isPlaying = MutableLiveData(false)
    val isPlaying: LiveData<Boolean>
        get() = _isPlaying

    private var _isPrepared = false
    private var _songUrl: String? = null

    override fun onPrepared(mp: MediaPlayer?) {
        _isPrepared = true
        if (_isPlaying.value == null) {
            _isPlaying.value = true
            mp?.start()
        }
    }

    override fun onError(mp: MediaPlayer?, what: Int, extra: Int): Boolean {
        Timber.e("Media Player error %d, extra: %d", what, extra)
        FirebaseCrashlytics.getInstance().setCustomKey("mp_what", what)
        FirebaseCrashlytics.getInstance().setCustomKey("mp_extra", extra)
        FirebaseCrashlytics.getInstance().log("MediaPlayer error")
        return false
    }

    override fun onCompletion(mp: MediaPlayer?) {
        _isPlaying.value = false
    }

    fun togglePlayPause() {
        when (_isPlaying.value) {
            true -> pauseSong()
            else -> playSong()
        }
    }

    fun pauseSong() {
        Timber.d("Pause song. Prepared[%b], Playing[%s]", _isPrepared, _isPlaying.value)
        _isPlaying.value = false
        if (mediaPlayer.isPlaying) {
            mediaPlayer.pause()
        }
    }

    fun playSong() {
        Timber.d("Play song. Prepared[%b], Playing[%s]", _isPrepared, _isPlaying.value)
        _songUrl?.let {
            if (_isPrepared) {
                _isPlaying.value = true
                mediaPlayer.start()
            } else {
                _isPlaying.value = null
                mediaPlayer.apply {
                    reset()
                    setAudioAttributes(mediaAttributes)
                    setDataSource(it)
                    prepareAsync()
                }
            }
        }
    }

    private suspend fun saveSongToDatabase(song: DatabaseSong) {
        withContext(Dispatchers.IO) {
            database.insert(song)
        }
    }

    fun saveSong(song: Song) {
        _songUrl = song.previewUrl
        scope.launch {
            saveSongToDatabase(song.asDatabaseModel())
        }
    }

    override fun onCleared() {
        super.onCleared()
        mediaPlayer.release()
        job.cancel()
    }
}
