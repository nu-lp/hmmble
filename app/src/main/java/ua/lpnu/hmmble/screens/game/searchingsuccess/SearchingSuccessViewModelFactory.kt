package ua.lpnu.hmmble.screens.game.searchingsuccess

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ua.lpnu.hmmble.database.SongDao

class SearchingSuccessViewModelFactory(private val database: SongDao) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SearchingSuccessViewModel::class.java)) {
            return SearchingSuccessViewModel(database) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}