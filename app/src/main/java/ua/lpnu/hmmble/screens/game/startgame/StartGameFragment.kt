package ua.lpnu.hmmble.screens.game.startgame

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import timber.log.Timber
import ua.lpnu.hmmble.R
import ua.lpnu.hmmble.database.GameDatabase
import ua.lpnu.hmmble.databinding.StartGameFragmentBinding


class StartGameFragment : Fragment() {
    companion object {
        const val PERMISSIONS_REQUEST_CODE = 11
    }

    private lateinit var viewModel: StartGameViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val dataSource = GameDatabase
            .getDatabase(requireActivity().application)
            .gameDao
        val factory = StartGameViewModelFactory(dataSource)
        viewModel = ViewModelProvider(this, factory).get(StartGameViewModel::class.java)
        val binding = StartGameFragmentBinding.inflate(inflater, container, false)

        viewModel.navigateToFirstTry.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().navigate(
                    StartGameFragmentDirections.actionStartGameFragmentToSearchFragment(1)
                )
                viewModel.doneNavigateToFirstTry()
            }
        })

        if (checkPermissions()) {
            viewModel.startGame()
        } else {
            requestPermissions()
        }
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewModel.cancelTimer()
    }

    private fun checkPermissions(): Boolean {
        Timber.d("Check permissions")
        return ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.RECORD_AUDIO
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermissions() {
        Timber.d("Request permissions")
        requestPermissions(arrayOf(Manifest.permission.RECORD_AUDIO), PERMISSIONS_REQUEST_CODE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        Timber.d(
            "Permissions result: code[%d], permissions[%s], results[%s]",
            requestCode,
            permissions.toList().toString(),
            grantResults.toList().toString()
        )
        if (requestCode == PERMISSIONS_REQUEST_CODE && grantResults.size == 1) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.RECORD_AUDIO)) {
                    Snackbar.make(
                        requireView(),
                        getString(R.string.application_requires_audio_permission),
                        Snackbar.LENGTH_INDEFINITE
                    )
                        .setAction(getString(R.string.grant)) {
                            requestPermissions()
                        }
                        .setActionTextColor(resources.getColor(R.color.colorAccent, null))
                        .show()
                } else {
                    Snackbar.make(
                        requireView(),
                        getString(R.string.grant_permission_in_settings),
                        Snackbar.LENGTH_INDEFINITE
                    )
                        .setAction(getString(R.string.snackbar_settings_action)) {
                            openApplicationSettings()
                        }
                        .setActionTextColor(resources.getColor(R.color.colorAccent, null))
                        .show()
                }
            } else {
                viewModel.startGame()
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun openApplicationSettings() {
        val appSettingsIntent = Intent(
            Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
            Uri.parse("package:" + requireActivity().packageName)
        )
        startActivityForResult(appSettingsIntent, PERMISSIONS_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Timber.d("Activity result: requestCode[%d], resultCode[%s]", requestCode, resultCode)
        if (requestCode == PERMISSIONS_REQUEST_CODE) {
            if (checkPermissions()) {
                viewModel.startGame()
            } else {
                requestPermissions()
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}