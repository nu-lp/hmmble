package ua.lpnu.hmmble.screens.game.startgame

import android.os.CountDownTimer
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import ua.lpnu.hmmble.database.DatabaseGame
import ua.lpnu.hmmble.database.GameDao

class StartGameViewModel(val gameDao: GameDao) : ViewModel() {
    companion object {
        const val TICK_INTERVAL = 500L
        const val COUNTDOWN_TIME = 2_000L
    }

    private val job = Job()
    private val scope = CoroutineScope(Dispatchers.Main + job)

    private val _startCountDown = MutableLiveData<Int>()
    val startCountDown: LiveData<Int>
        get() = _startCountDown

    private val _navigateToFirstTry = MutableLiveData<Boolean>()
    val navigateToFirstTry: LiveData<Boolean>
        get() = _navigateToFirstTry

    private val timer = object : CountDownTimer(COUNTDOWN_TIME, TICK_INTERVAL) {
        override fun onFinish() {
            _navigateToFirstTry.value = true
            scope.launch {
                createNewGame()
            }
        }

        override fun onTick(millisUntilFinished: Long) {
            _startCountDown.value = (millisUntilFinished / TICK_INTERVAL).toInt()
        }
    }

    fun startGame() {
        timer.start()
    }

    fun cancelTimer() {
        timer.cancel()
    }

    private suspend fun createNewGame() {
        withContext(Dispatchers.IO) {
            gameDao.insert(DatabaseGame())
        }
    }

    fun doneNavigateToFirstTry() {
        _navigateToFirstTry.value = false
    }

    override fun onCleared() {
        super.onCleared()
        timer.cancel()
        job.cancel()
    }
}