package ua.lpnu.hmmble.screens.game.startgame

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ua.lpnu.hmmble.database.GameDao

class StartGameViewModelFactory(val gameDao: GameDao) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(StartGameViewModel::class.java)) {
            return StartGameViewModel(gameDao) as T
        }
        throw IllegalArgumentException("Unknown view model")
    }
}