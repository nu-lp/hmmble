package ua.lpnu.hmmble.screens.game.userwon

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import ua.lpnu.hmmble.database.GameDatabase
import ua.lpnu.hmmble.databinding.UserWonFragmentBinding

class UserWonFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val dataSource = GameDatabase
            .getDatabase(requireActivity().application)
            .gameDao
        val factory = UserWonViewModelFactory(dataSource)
        val viewModel = ViewModelProvider(this, factory).get(UserWonViewModel::class.java)
        val binding = UserWonFragmentBinding.inflate(inflater, container, false)

        binding.newGameBtn.setOnClickListener {
            findNavController().navigate(UserWonFragmentDirections.actionUserWonFragmentToStartGameFragment())
        }
        viewModel.userWonGame()

        return binding.root
    }
}
