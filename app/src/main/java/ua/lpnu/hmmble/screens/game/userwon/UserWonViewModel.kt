package ua.lpnu.hmmble.screens.game.userwon

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import ua.lpnu.hmmble.database.GameDao

class UserWonViewModel(val gameDao: GameDao) : ViewModel() {
    private val job = Job()
    private val scope = CoroutineScope(Dispatchers.Main + job)

    private suspend fun updateGameInDatabase() {
        withContext(Dispatchers.IO) {
            val game = gameDao.getLastGame()
            game.isUserWon = true
            gameDao.update(game)
        }
    }

    fun userWonGame() {
        scope.launch {
            updateGameInDatabase()
        }
    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
}