package ua.lpnu.hmmble.screens.game.userwon

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ua.lpnu.hmmble.database.GameDao

class UserWonViewModelFactory(val gameDao: GameDao) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(UserWonViewModel::class.java)) {
            return UserWonViewModel(gameDao) as T
        }
        throw IllegalArgumentException("Unknown view model")
    }
}