package ua.lpnu.hmmble.screens.score

import android.widget.TextView
import androidx.databinding.BindingAdapter

@BindingAdapter("number")
fun TextView.displayNumber(number: Long) {
    this.text = number.toString()
}