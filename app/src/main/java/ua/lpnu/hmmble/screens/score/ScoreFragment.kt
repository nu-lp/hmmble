package ua.lpnu.hmmble.screens.score

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import ua.lpnu.hmmble.R
import ua.lpnu.hmmble.database.GameDatabase
import ua.lpnu.hmmble.databinding.ScoreFragmentBinding

class ScoreFragment : Fragment() {
    private lateinit var viewModel: ScoreViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val dataSource = GameDatabase
            .getDatabase(requireActivity().application)
            .gameDao
        val factory = ScoreViewModelFactory(dataSource)
        viewModel = ViewModelProvider(this, factory).get(ScoreViewModel::class.java)
        val binding = ScoreFragmentBinding.inflate(inflater, container, false)

        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.score_options_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.resetScore) {
            viewModel.resetScore()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
