package ua.lpnu.hmmble.screens.score

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import ua.lpnu.hmmble.database.GameDao

class ScoreViewModel(val gameDao: GameDao) : ViewModel() {
    private val job = Job()
    private val scope = CoroutineScope(Dispatchers.Main + job)

    val score = gameDao.getScore()

    private suspend fun resetDatabaseScore() {
        withContext(Dispatchers.IO) {
            gameDao.resetScore()
        }
    }

    fun resetScore() {
        scope.launch {
            resetDatabaseScore()
        }
    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
}
