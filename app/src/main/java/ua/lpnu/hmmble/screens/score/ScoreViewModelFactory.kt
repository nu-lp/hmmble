package ua.lpnu.hmmble.screens.score

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ua.lpnu.hmmble.database.GameDao

class ScoreViewModelFactory(val gameDao: GameDao) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ScoreViewModel::class.java)) {
            return ScoreViewModel(gameDao) as T
        }
        throw IllegalArgumentException("Unknown view model")
    }
}