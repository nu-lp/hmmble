package ua.lpnu.hmmble.screens.songshistory

import android.content.res.ColorStateList
import android.widget.ImageButton
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import ua.lpnu.hmmble.R

@BindingAdapter("viewMode", "itemViewMode")
fun ImageButton.colorButton(viewMode: ViewMode, itemViewMode: ViewMode) {
    val color = when (viewMode) {
        itemViewMode -> R.color.colorAccent
        else -> R.color.colorSecondary
    }
    imageTintList = ColorStateList.valueOf(ContextCompat.getColor(context, color))

}