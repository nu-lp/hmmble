package ua.lpnu.hmmble.screens.songshistory

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import ua.lpnu.hmmble.databinding.GridItemSongBinding
import ua.lpnu.hmmble.databinding.ListItemSongBinding
import ua.lpnu.hmmble.domain.Song

class SongItemClickListener(val clickListener: (songId: Long) -> Unit) {
    fun onClick(song: Song) = clickListener(song.id)
}

class SongItemAdapter(
    private val layoutManager: GridLayoutManager,
    private val moreBtnClickListener: SongItemClickListener
) :
    ListAdapter<Song, RecyclerView.ViewHolder>(DiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            ViewMode.LIST.ordinal -> ListViewHolder.from(parent)
            else -> GridViewHolder.from(parent)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val song = getItem(position)
        when (holder) {
            is ListViewHolder -> holder.bind(song, moreBtnClickListener)
            is GridViewHolder -> holder.bind(song, moreBtnClickListener)
            else -> throw IllegalArgumentException("Unknown view holder for position: $position")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (layoutManager.spanCount) {
            1 -> ViewMode.LIST.ordinal
            else -> ViewMode.GRID.ordinal
        }
    }

    companion object DiffCallback : DiffUtil.ItemCallback<Song>() {
        override fun areItemsTheSame(oldItem: Song, newItem: Song): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Song, newItem: Song): Boolean {
            return oldItem == newItem
        }
    }
}

class ListViewHolder private constructor(val binding: ListItemSongBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind(
        item: Song,
        moreBtnClickListener: SongItemClickListener
    ) {
        binding.song = item
        binding.moreBtnClickListener = moreBtnClickListener
        binding.executePendingBindings()
    }

    companion object {
        fun from(parent: ViewGroup): ListViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = ListItemSongBinding.inflate(inflater, parent, false)
            return ListViewHolder(binding)
        }
    }
}

class GridViewHolder private constructor(val binding: GridItemSongBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind(
        item: Song,
        moreBtnClickListener: SongItemClickListener
    ) {
        binding.song = item
        binding.moreBtnClickListener = moreBtnClickListener
        binding.executePendingBindings()
    }

    companion object {
        fun from(parent: ViewGroup): GridViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = GridItemSongBinding.inflate(inflater, parent, false)
            return GridViewHolder(binding)
        }
    }
}

