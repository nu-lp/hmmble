package ua.lpnu.hmmble.screens.songshistory

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import ua.lpnu.hmmble.R
import ua.lpnu.hmmble.database.GameDatabase
import ua.lpnu.hmmble.databinding.SongsHistoryFragmentBinding
import ua.lpnu.hmmble.elements.songopenin.OpenInBottomSheetFragment

class SongsHistoryFragment : Fragment() {
    private lateinit var viewModel: SongsHistoryViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val layoutManager = GridLayoutManager(activity, 1)
        val adapter = SongItemAdapter(
            layoutManager,
            getMoreBtnSongItemClickListener()
        )
        val dataSource = GameDatabase.getDatabase(requireActivity().application)
        val factory = SongsHistoryViewModelFactory(dataSource.songDao)
        viewModel = ViewModelProvider(this, factory).get(SongsHistoryViewModel::class.java)
        val binding = SongsHistoryFragmentBinding.inflate(inflater, container, false)

        viewModel.songs.observe(viewLifecycleOwner, Observer {
            adapter.submitList(it)
        })
        viewModel.viewMode.observe(viewLifecycleOwner, Observer {
            when (it) {
                ViewMode.LIST -> layoutManager.spanCount = 1
                else -> layoutManager.spanCount = 2
            }
            adapter.notifyItemRangeChanged(0, adapter.itemCount)
        })

        binding.lifecycleOwner = viewLifecycleOwner
        binding.songList.adapter = adapter
        binding.songList.layoutManager = layoutManager
        binding.viewModel = viewModel
        setHasOptionsMenu(true)

        return binding.root
    }

    private fun getMoreBtnSongItemClickListener() = SongItemClickListener {
        // Bottom sheet
        val openInArgs = Bundle()
        openInArgs.putLong(OpenInBottomSheetFragment.SONG_ID_ARG, it)

        val openIn = OpenInBottomSheetFragment()
        openIn.arguments = openInArgs
        openIn.show(childFragmentManager, OpenInBottomSheetFragment.TAG)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.song_history_options_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.clearHistory) {
            viewModel.clearHistory()
        }
        return super.onOptionsItemSelected(item)
    }
}
