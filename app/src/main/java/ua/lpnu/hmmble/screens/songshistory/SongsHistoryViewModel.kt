package ua.lpnu.hmmble.screens.songshistory

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import ua.lpnu.hmmble.database.SongDao
import ua.lpnu.hmmble.database.asDomainModel

enum class ViewMode {
    LIST,
    GRID
}

class SongsHistoryViewModel(val database: SongDao) : ViewModel() {
    private val job = Job()
    private val scope = CoroutineScope(job + Dispatchers.Main)

    private val _songs = database.getAllSongs()
    val songs = Transformations.map(_songs) {
        it.map {
            it.asDomainModel()
        }
    }

    private val _viewMode = MutableLiveData(ViewMode.LIST)
    val viewMode: LiveData<ViewMode>
        get() = _viewMode

    fun setViewMode(mode: ViewMode) {
        if (_viewMode.value != mode) {
            _viewMode.value = mode
        }
    }

    private suspend fun clearDatabaseHistory() {
        withContext(Dispatchers.IO) {
            database.clearSongsHistory()
        }
    }

    fun clearHistory() {
        scope.launch {
            clearDatabaseHistory()
        }
    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
}
