package ua.lpnu.hmmble.screens.songshistory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ua.lpnu.hmmble.database.SongDao

class SongsHistoryViewModelFactory(val database: SongDao) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SongsHistoryViewModel::class.java)) {
            return SongsHistoryViewModel(database) as T
        }
        throw IllegalArgumentException("Unknown view model")
    }
}
