package ua.lpnu.hmmble.utils

import android.widget.ImageView
import androidx.core.net.toUri
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import ua.lpnu.hmmble.R

@BindingAdapter("imageUrl")
fun ImageView.bindImage(url: String?) {
    if (null != url) {
        val uri = url.toUri().buildUpon().scheme("https").build()
        Glide.with(context)
            .applyDefaultRequestOptions(
                RequestOptions()
                    .placeholder(R.drawable.loading_anim)
                    .error(R.drawable.ic_broken_image_black_24dp)
            )
            .load(uri)
            .into(this)
    } else {
        setImageResource(R.drawable.ic_broken_image_black_24dp)
    }
}