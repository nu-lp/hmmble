package ua.lpnu.hmmble.utils

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import timber.log.Timber
import ua.lpnu.hmmble.receivers.GameReminderReceiver

class GameReminderUtil {
    companion object {
        private const val THREE_DAYS = 3L * 24L * 60L * 60L * 1000L
        private const val REQUEST_CODE = 2020

        fun setupReminder(context: Context) {
            val pendingIntent = Intent(context, GameReminderReceiver::class.java).run {
                PendingIntent.getBroadcast(context, REQUEST_CODE, this, 0)
            }
            val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager

            alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + THREE_DAYS, pendingIntent)

            Timber.d("Game reminder has been setup")
        }

        fun cancelReminder(context: Context) {
            val pendingIntent = Intent(context, GameReminderReceiver::class.java).run {
                PendingIntent.getBroadcast(context, REQUEST_CODE, this, 0)
            }
            val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            alarmManager.cancel(pendingIntent)

            Timber.d("Game reminder has been canceled")
        }

    }
}