package ua.lpnu.hmmble.utils

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import ua.lpnu.hmmble.R


class NotificationUtil {
    companion object {
        const val GAME_REMINDER_ID = 1

        fun createNotificationChannel(context: Context, notificationData: NotificationData): String {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val notificationChannel = notificationData.run {
                    NotificationChannel(channelId, channelName, channelImportance).also {
                        it.description = channelDescription
                        it.enableVibration(channelEnabledVibration)
                        it.lockscreenVisibility = channelLockScreenVisibility
                    }
                }
                // Adds NotificationChannel to system. Attempting to create an existing notification
                // channel with its original values performs no operation, so it's safe to perform the
                // below sequence.
                val notificationManager =
                    context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                notificationManager.createNotificationChannel(notificationChannel)
            }
            return notificationData.channelId
        }
    }

    data class NotificationData(
        // Android O and above
        var channelId: String,
        val channelName: String,
        var channelDescription: String,
        var channelImportance: Int,
        var channelEnabledVibration: Boolean,
        var channelLockScreenVisibility: Int
    ) {
        companion object {
            fun createGameReminder(context: Context): NotificationData {
                return NotificationData(
                    "channel_play_reminder12",
                    context.getString(R.string.game_reminder_channel),
                    context.getString(R.string.game_reminder_channed_description),
                    NotificationManagerCompat.IMPORTANCE_HIGH,
                    true,
                    NotificationCompat.VISIBILITY_PUBLIC
                )
            }
        }
    }
}